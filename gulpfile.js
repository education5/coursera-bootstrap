'use strict';

const gulp = require('gulp'),
    sass = require('gulp-sass'),
    del = require('del'),
    imagemin = require('gulp-imagemin'),
    browserSync = require('browser-sync'),
    uglify = require('gulp-uglify'),
    usemin = require('gulp-usemin'),
    rev = require('gulp-rev'),
    cleanCss = require('gulp-clean-css'),
    flatmap = require('gulp-flatmap'),
    htmlmin = require('gulp-htmlmin');

function sassTask() {
  return gulp.src('./css/*.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(gulp.dest('./css'));
}

function sassWatch() {
  gulp.watch('./css/*.scss', sassTask);
}

function browserSyncTask() {
   var files = [
      './*.html',
      './css/*.css',
      './img/*.{png,jpg,gif}',
      './js/*.js'
   ];

   browserSync.init(files, {
      server: {
         baseDir: "./"
      }
   });
}

// Clean
function clean() {
    return del(['dist']);
}

function copyfonts() {
   return (
       gulp
       .src('./node_modules/font-awesome/fonts/**/*.{ttf,woff,eof,svg}*')
       .pipe(gulp.dest('./dist/fonts'))
   );
}

// Images
function imageminTask() {
  return (
      gulp
      .src('img/*.{png,jpg,gif}')
      .pipe(
          imagemin({
                  optimizationLevel: 3,
                  progressive: true,
                  interlaced: true
          })
      )
      .pipe(gulp.dest('dist/img'))
  );
}

function useminTask() {
  return (
      gulp
      .src('./*.html')
      .pipe(flatmap(function(stream, file){
          return stream
          .pipe(
              usemin({
                css: [ rev() ],
                html: [ function() { return htmlmin({ collapseWhitespace: true })} ],
                js: [ uglify(), rev() ],
                inlinejs: [ uglify() ],
                inlinecss: [ cleanCss(), 'concat' ]
              })
          )
      }))
      .pipe(gulp.dest('dist/'))
  );
}

const watch = gulp.series(browserSyncTask, sassWatch);
const build = gulp.series(clean, gulp.parallel(copyfonts, imageminTask, useminTask));

exports.clean = clean
exports.watch = watch;
exports.build = build;
